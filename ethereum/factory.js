import web3 from "./web3";

import CampaignFactory from "./build/CampaignFactory.json";

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  "0x336EC6410baB0842a75F3e0243e27e34FDdc76de"
);

export default instance;
