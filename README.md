# BlockchainProject-kickstart
Blockchain project to learn solity and react.
Kickstarter clone allowing project contributors to appove spending of project owner. 
This stops project owners creating projects to raise funds and then not have any intention of finishing the project and taking the funds.

Tech stack:
Solity
Rinkeby Ethereum Test Network
React & key libraries: semantic-ui-react
